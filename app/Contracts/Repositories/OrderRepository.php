<?php

namespace App\Contracts\Repositories;

use App\Models\Order;

interface OrderRepository
{
    public function store($foodId) : Order;
}
