<?php

namespace App\Contracts\Repositories;

use App\Models\Ingredients;
use Illuminate\Database\Eloquent\Collection;

interface IngredientsRepository
{
    public function finishedItems() : Collection;
    public function update(Ingredients $ingredients, array $attributes) : bool;
    public function decreaseStocks($foodId) : void;
}
