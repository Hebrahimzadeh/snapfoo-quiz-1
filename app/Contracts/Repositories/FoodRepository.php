<?php

namespace App\Contracts\Repositories;

use App\Models\Food;
use  Illuminate\Support\Collection;

interface FoodRepository
{
    public function availableFoods() : Collection;
    public function foodInfo($id) : Collection;
}
