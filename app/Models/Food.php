<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Food extends Model
{
    use HasFactory;
    protected $table = 'foods';


    public function ingredients(){
        return $this->belongsToMany(Ingredients::class, 'food_ingredients', 'food_id', 'ingredients_id')->get();
    }

}
