<?php

namespace App\Http\Requests;


use App\Contracts\Repositories\FoodRepository;

class StoreOrderRequest extends ApiFormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'food_id' => [
                'required', 'exists:foods,id', function ($attribute, $foodId, $fail) {
                    if (! $this->isFoodAvailable($foodId)){
                        $fail(trans('validation.exists', ['attribute' => $attribute]));
                    }
                }
            ]
        ];
    }

    private function isFoodAvailable($foodId) : bool
    {
        $foodRecord = app(FoodRepository::class)->foodInfo($foodId)->first();
        if ($foodRecord === null){
            return false;
        }
        return $foodRecord->food_ingredients_count === $foodRecord->food_ingredients_available_count;
    }
}
