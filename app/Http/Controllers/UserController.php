<?php

namespace App\Http\Controllers;

use App\Http\Requests\LoginRequest;
use App\Models\User;
use Illuminate\Http\Response;
use Illuminate\Validation\ValidationException;

class UserController extends Controller
{
    public function login(LoginRequest $request)
    {
        $user = User::where('email', $request->get('email'))->first();

        if (!$user || !\Hash::check($request->get('password'), $user->password)) {
            throw new ValidationException(null, response([
                'error_message' => 'The provided credentials are incorrect.',
                'errors' => [
                    'email' => ['The provided credentials are incorrect.']
                ]
            ], Response::HTTP_UNPROCESSABLE_ENTITY));
        }
        $user->tokens()->delete();
        return 'your token is: ' . $user->createToken('admin')->plainTextToken;
    }
}
