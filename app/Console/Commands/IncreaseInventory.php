<?php

namespace App\Console\Commands;

use App\Contracts\Repositories\IngredientsRepository;
use App\Models\Ingredients;
use Illuminate\Console\Command;

class IncreaseInventory extends Command
{
    protected IngredientsRepository $ingredientsRepository;
    protected \DateTime $today;
    public const INCREASING_NUMBER = 4;
    public const DATE_FORMAT = 'Y-m-d';
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'inventory:increase';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Increase the ingredients finished.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct(IngredientsRepository $ingredientsRepository)
    {
        parent::__construct();
        $this->ingredientsRepository = $ingredientsRepository;
        $this->today = new \DateTime();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $this->info('I will increasing finished ingredients.');
        $fItems = $this->ingredientsRepository->finishedItems();
        if (!$fItems->count()) {
            $this->info('Ingredients that finished, not found.');
            return 0;
        }

        $fItems->each(function ($item) {
            $newExpiresAt = $this->newExpiresDate($item);
            if ($newExpiresAt !== null) {
                $this->increase($item, $newExpiresAt);
            }
        });

        return 0;
    }

    /**
     * @param  Ingredients  $item
     * @return string|null
     * @throws \Exception
     */
    private function newExpiresDate(Ingredients $item): ?string
    {
        try {
            $bestBefore = new \DateTime($item->best_before);
            $expiresAt = new \DateTime($item->expires_at);

            $expiresIngredientByDay = $bestBefore->diff($expiresAt)->days;
            $newExpiresDate = clone $this->today;
            $newExpiresDate->modify("+{$expiresIngredientByDay} days");
            $newExpires = $newExpiresDate->format(self::DATE_FORMAT);

            $this->info($item->title . ', best_before and expires_at diff is: ' . $expiresIngredientByDay . ' days, new expires date is: ' . $newExpires);

            return $newExpires;
        } catch (\Exception $exception) {
            \Log::error(
                'I received exception on calculator (newExpireDate) ingredients time. by message: '.$exception->getMessage()
            );
        }
        return null;
    }

    /**
     * @param  Ingredients  $item
     * @param  string  $expiresAt
     * @return bool
     */
    private function increase(Ingredients $item,string $expiresAt): bool
    {
        $this->ingredientsRepository->update(
            $item,
            [
                'best_before' => $this->today->format(self::DATE_FORMAT),
                'expires_at' => $expiresAt,
                'stock' => self::INCREASING_NUMBER,
            ]
        );

        $this->info(self::INCREASING_NUMBER.$item->title.' were added.');
        return $item->save();
    }

    public function info($string, $verbosity = null)
    {
        info($string);
        parent::info($string);
    }
}
