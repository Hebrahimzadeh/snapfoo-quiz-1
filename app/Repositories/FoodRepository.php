<?php

namespace App\Repositories;

use App\Contracts\Repositories\FoodRepository as FoodRepositoryContract;
use DB;
use  Illuminate\Support\Collection;
use Illuminate\Database\Query\Builder;

class FoodRepository implements FoodRepositoryContract
{

    private string $toady;

    public function __construct()
    {
        $this->toady = date('Y-m-d');
    }

    public function availableFoods(): Collection
    {
//        DB::enableQueryLog();
        $sub = DB::table("foods")
            ->select('id', 'title')
            ->addSelect(DB::raw($this->foodsIngredientsCountSql()))
            ->addSelect(DB::raw($this->foodsIngredientsAvailableSql($this->toady)))
            ->addSelect(DB::raw($this->foodsIngredientsSortByDatesSql()));

        $cols = DB::table(DB::raw("({$sub->toSql()}) as foods, (SELECT @row_num:=0) row_num"))
            ->select(DB::raw('id, `title`, @row_num:=@row_num+1 as `order`'))
            ->whereColumn('food_ingredients_count', '=', 'food_ingredients_available_count')
            ->orderBy('dates', 'DESC')
            ->get();
//        dd(DB::getQueryLog(), $cols);
        return $cols;
    }

    private function foodsIngredientsCountSql(): string
    {
        return '(
            SELECT COUNT( `food_id` ) FROM `food_ingredients` AS `rel` WHERE `rel`.`food_id` = `foods`.`id`
        ) AS `food_ingredients_count`';
    }

    private function foodsIngredientsAvailableSql(string $date, ?int $foodId = null): string
    {
        return "
        (
            SELECT
            COUNT( `food_id` )
            FROM
                `food_ingredients` AS `rel`
                JOIN `ingredients` AS ing ON ( `rel`.`food_id` = `foods`.`id` AND `ing`.`id` = `rel`.`ingredients_id` )
            WHERE
                ing.expires_at >= '{$date}'
                AND ing.stock > 0
        ) AS `food_ingredients_available_count`";
    }

    private function foodsIngredientsSortByDatesSql(): string
    {
        return '(
		SELECT
			GROUP_CONCAT( `best_before` ORDER BY `best_before` ASC )
		FROM
			(
			SELECT
				`ing`.`best_before`
			FROM
				`food_ingredients` AS `rel`
				JOIN `ingredients` AS `ing` ON ( `rel`.`food_id` = `foods`.`id` AND `ing`.`id` = `rel`.`ingredients_id` )
			) AS `dates`
		) AS `dates` ';
    }


    public function foodInfo($id) : Collection
    {
        return
            DB::table('foods')
                ->select('title')
                ->addSelect(DB::raw($this->foodsIngredientsCountSql()))
                ->addSelect(DB::raw($this->foodsIngredientsAvailableSql($this->toady)))
                ->where('id', $id)
                ->get();
    }
}
