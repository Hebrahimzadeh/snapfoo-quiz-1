<?php

namespace App\Repositories;

use \App\Contracts\Repositories\OrderRepository as OrderRepositoryContract;
use App\Models\Order;

class OrderRepository implements OrderRepositoryContract
{
    public function store($foodId) : Order{
        $data = [
            'food_id' => $foodId
        ];
        return Order::create($data);
    }
}
