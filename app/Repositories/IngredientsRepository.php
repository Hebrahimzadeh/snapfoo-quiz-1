<?php

namespace App\Repositories;

use App\Contracts\Repositories\IngredientsRepository as IngredientsRepositoryContract;
use App\Models\Food;
use App\Models\Ingredients;
use Illuminate\Database\Eloquent\Collection;

class IngredientsRepository implements IngredientsRepositoryContract
{

    public function finishedItems(): Collection{
        return Ingredients::where('stock', '0')->get();
    }

    public function update(Ingredients $ingredients,array $attributes): bool
    {
        $ingredients->setAttribute('best_before', $attributes['best_before'] );
        $ingredients->setAttribute('expires_at', $attributes['expires_at']);
        $ingredients->setAttribute('stock', $attributes['stock']);
        return $ingredients->save();
    }

    public function decreaseStocks($foodId): void
    {
        $food = Food::find($foodId);
        $food->ingredients()->each(function (Ingredients $ingredients){
            $ingredients->setAttribute('stock', $ingredients->getOriginal('stock')-1);
            $ingredients->save();
        });
    }
}
