<?php

namespace App\Providers;

use App\Contracts\Repositories\FoodRepository as FoodRepositoryContract;
use App\Contracts\Repositories\IngredientsRepository as IngredientsRepositoryContract;
use App\Repositories\IngredientsRepository;
use App\Contracts\Repositories\OrderRepository as OrderRepositoryContract;
use App\Repositories\FoodRepository;
use App\Repositories\OrderRepository;
use Illuminate\Support\ServiceProvider;

class RepositoryServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(FoodRepositoryContract::class, FoodRepository::class);
        $this->app->bind(OrderRepositoryContract::class, OrderRepository::class);
        $this->app->bind(IngredientsRepositoryContract::class, IngredientsRepository::class);
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
