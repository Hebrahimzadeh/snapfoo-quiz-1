<?php

namespace Tests;

use App\Models\Ingredients;
use App\Models\User;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\TestCase as BaseTestCase;

abstract class TestCase extends BaseTestCase
{
    use CreatesApplication, DatabaseMigrations, RefreshDatabase;
    protected array $connectionsToTransact = ['mysql'];

    protected function setUp(): void
    {

        parent::setUp();

        $user = User::find(1);
        if ($user === null) {
            $user = User::factory(1)->uniqueEmail()->create(['id' => 1])->first();
        }

        $this->actingAs($user);
        $this->seed();
    }



    protected function foodPreparation(){
        Ingredients::all()->each(function ($ingredients){
            $ingredients->update(['stock' => 0]);
        });
        $this->artisan('inventory:increase');
    }
}
