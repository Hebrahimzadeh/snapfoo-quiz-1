<?php

namespace Tests\Feature;

use App\Models\Ingredients;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Testing\Fluent\AssertableJson;
use Symfony\Component\HttpFoundation\Response;
use Tests\TestCase;

class MenuTest extends TestCase
{
    /**
     * A basic feature test example.
     *
     * @return void
     */
    final public function test_foods(): void
    {
        $response = $this->get('/api/menu');
        $response->assertStatus(Response::HTTP_NOT_FOUND);
    }

    final public function test_menu_food_available()
    {
        $this->foodPreparation();

       $response = $this->get('/api/menu');
        $response->assertStatus(Response::HTTP_OK);
        $response->assertJson(fn(AssertableJson $json) =>
            $json->hasAll('data')
        );

        $response->assertJsonFragment(
            ['id' => 1]
        );
    }

}
