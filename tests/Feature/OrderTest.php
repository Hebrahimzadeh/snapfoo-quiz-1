<?php

namespace Tests\Feature;

use App\Models\Ingredients;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;
use Symfony\Component\HttpFoundation\Response;


class OrderTest extends TestCase
{
    /**
     * A basic feature test example.
     *
     * @return void
     */
    final public function test_register_order_ingredients_expired(): void
    {
        $response = $this->post('api/order', ['food_id' => 1]);
        $response->assertStatus(Response::HTTP_UNPROCESSABLE_ENTITY);
    }

    final public function test_register_order(): void
    {
        $this->foodPreparation();
        $response = $this->post('api/order', ['food_id' => 1]);
        $response->assertStatus(Response::HTTP_OK);
    }
}
