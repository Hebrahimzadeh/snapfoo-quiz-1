<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use DB;
use mysql_xdevapi\Exception;

class IngredientsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        foreach ($this->getData() as $item) {
            DB::table('ingredients')->insertOrIgnore($item);
        }
    }

    public function getData() : array
    {
        try {
            $data = json_decode(file_get_contents(__DIR__ . '/ingredients.json'), true, 512, JSON_THROW_ON_ERROR);
            return $data['ingredients'];
        }catch (\JsonException $exception){

        }
        return [];
    }
}
