<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use DB;

class FoodSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run() : void
    {
        $foods = $this->getData();
        foreach ($foods as $item) {
            unset($item['ingredients']);
            DB::table('foods')->insertOrIgnore($item);
        }
    }

    public function getData() : array
    {
        try {
            $data = json_decode(file_get_contents(__DIR__ . '/foods.json'), true, 512, JSON_THROW_ON_ERROR);
            return $data['recipes'];
        }catch (\JsonException $exception){

        }
        return [];
    }
}
