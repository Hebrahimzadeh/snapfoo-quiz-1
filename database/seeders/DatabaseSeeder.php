<?php

namespace Database\Seeders;

use App\Models\User;
use Illuminate\Database\Seeder;
use Cache;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {

        $this->call([
                IngredientsSeeder::class,
                FoodSeeder::class,
                FoodIngredientsSeeder::class,
            ]);

        $user = User::find(1);
        if ($user === null) {
            $user = User::factory(1)->uniqueEmail()->create(['id' => 1])->first();
        }

        $user->tokens()->delete();
        $token = $user->createToken('admin')->plainTextToken;
        $this->command->info('Primitive data initialized.');
        $this->command->info('created your user with this info: email: ' . $user->email .  ' password: password');
        $this->command->info('your token is: '.$token);

    }
}
