<?php

namespace Database\Seeders;

use App\Models\Food;
use App\Models\Ingredients;
use Illuminate\Database\Seeder;
use DB;

class FoodIngredientsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $foods = $this->foodData();
        foreach ($foods as $foodData){
            $food = $this->foodByTitle($foodData['title']);
            if ($food === null) {
                continue;
            }

            foreach ($foodData['ingredients'] as $ingredient){
                $ingredient = $this->ingredientByTitle($ingredient);
                if ($ingredient instanceof Ingredients){
                    $this->addFoodIngredients($ingredient, $food);
                }
            }
        }
    }

    private function foodData() : array
    {
        try {
            $data = json_decode(file_get_contents(__DIR__ . '/foods.json'), true, 512, JSON_THROW_ON_ERROR);
            return $data['recipes'];
        }catch (\JsonException $exception){

        }
        return [];
    }

    private function addFoodIngredients(Ingredients $ingredients, Food $food){
        DB::table('food_ingredients')->insertOrIgnore(
            [
                'food_id' => $food->id,
                'ingredients_id' => $ingredients->id,
            ]
        );
    }

    private function ingredientByTitle($title) : Ingredients|null
    {
        try{
            return Ingredients::where('title', $title)->firstOrFail();
        }catch (\Exception $exception){

        }
        return null;
    }

    private function foodByTitle($title) : Food|null
    {
        try{
            return Food::where('title', $title)->firstOrFail();
        }catch (\Exception $exception){

        }
        return null;
    }
}
